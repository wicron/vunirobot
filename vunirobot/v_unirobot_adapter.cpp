/*
 *  Copyright (c) 2012-2013 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "v_unirobot_global.h"
#include "v_unirobot_adapter.h"

#include <cassert>
#include <fstream>

#include <vcore/macros.h>

enum { SET_PORT_PARAMETR_TIMEOUT = 10 }; // millisec
enum { SLEEP_UNIROBOT = 65000 };
enum { WATCH_UNIROBOT = 0 };

using namespace vunirobot;

static_assert(sizeof(ReadData)  == VUnirobotAdapter::READ_DATA_LENGHT  * 2, "Invalid size of read data");
static_assert(sizeof(WriteData) == VUnirobotAdapter::WRITE_DATA_LENGHT * 2, "Invalid size of write data");

const int VUnirobotAdapter::MOTOR_SPEED_MIN = -800;
const int VUnirobotAdapter::MOTOR_SPEED_MAX =  800;

const char VUnirobotAdapter::PARITY = 'E';

enum { RESET_MAGNETIC_TRIGGER     = 1251 };
enum { RESET_MAGNETIC_TRIGGER_KEY = 13 };

enum { CONFIG_KEY_REGISTER = 1250 };
enum { CONFIG_KEY_PREPARE = 13 };
enum { CONFIG_KEY_WRITE   = 219 };

VUnirobotAdapter::VUnirobotAdapter() noexcept :
    VModbusRTUMaster()
{
    m_read_data.reset( new vunirobot::ReadData( ) );
    m_write_data.reset( new vunirobot::WriteData( ) );

    size_t i;
    for( i = 0; i < MOTOR_NUM; ++i ) {
        m_write_data->motionType[i] = SPEED_MODE;
    }
    for( i = 0; i < MOTOR_NUM; ++i ) {
        m_write_data->motorDirection[i] = DIRECT;
    }
}

VUnirobotAdapter::~VUnirobotAdapter()
{
}

bool VUnirobotAdapter::connect(const std::string& portName) noexcept
{
    setTimeout( TIMEOUT, TIMEOUT );
    bool res = VModbusRTUMaster::connect(portName.c_str(), BAUDRATE, PARITY, DATA_BIT, STOP_DIR);
    setSlaveId(BOARD_ID);
    m_portName = portName;
    return res;
}

const std::string& VUnirobotAdapter::getPortName() const noexcept
{
    return m_portName;
}

bool VUnirobotAdapter::readData()
{
    return VModbusRTUMaster::readRegisters(READ_START_REGISTER, READ_DATA_LENGHT, reinterpret_cast<uint16_t*>(m_read_data.get()));
}

bool VUnirobotAdapter::writeData()
{
    return VModbusRTUMaster::writeRegisters(WRITE_START_REGISTERS, WRITE_DATA_LENGHT, reinterpret_cast<uint16_t*>(m_write_data.get()));
}

void VUnirobotAdapter::getData(vunirobot::ReadData& data) const noexcept
{
    data = *m_read_data.get();
}

int VUnirobotAdapter::getBoardPower() const noexcept
{
    return m_read_data->boardPower;
}

bool VUnirobotAdapter::isCharging() const noexcept
{
    return m_read_data->boardState;
}

void VUnirobotAdapter::getSenorsValue(vunirobot::sensors_data& data) const noexcept
{
    data.resize(vunirobot::SENSORS_NUM, vunirobot::DEFAULT_VALUE);

    for(size_t i = 0; i < vunirobot::SENSORS_NUM; ++i) {
        data[i] = m_read_data->sensorState[i];
    }
}

void VUnirobotAdapter::getMotorSpeed(vunirobot::motor_speed& data) const noexcept
{
    data.resize(vunirobot::MOTOR_NUM, vunirobot::DEFAULT_VALUE);

    for(size_t i = 0; i < vunirobot::MOTOR_NUM; ++i) {
        data[i] = m_read_data->encoder[i];
    }
}

void VUnirobotAdapter::getMagnet(vunirobot::magnet_data& data) const noexcept
{
    data.resize(3);
    data[0] = m_read_data->magnetX;
    data[1] = m_read_data->magnetY;
    data[2] = m_read_data->magnetZ;
}

void VUnirobotAdapter::getAxel(vunirobot::axel_data& data) const noexcept
{
    data.resize(3);
    data[0] = m_read_data->axelX;
    data[1] = m_read_data->axelY;
    data[2] = m_read_data->axelZ;
}

bool VUnirobotAdapter::getMagnetState() const noexcept
{
    return m_read_data->magnetState;
}

void VUnirobotAdapter::getRobotParameters(vunirobot::RobotParameters& param) const noexcept
{
    param.distanceBetweenWheels = m_read_data->distanceBetweenWheels;
    param.wheelRadius = m_read_data->wheelRadius;
    param.encoderTicksPerRotation = m_read_data->encoderTicksPerRotation;
    param.reductionRatio = m_read_data->reductionRatio;
}

void VUnirobotAdapter::getRobotCoordinate(vunirobot::RobotCoordinate& coord) const noexcept
{
    coord.coordinate_x = m_read_data->coordinate_x;
    coord.coordinate_y = m_read_data->coordinate_y;
    coord.coordinate_angle = m_read_data->coordinate_angle;
    coord.coordinate_omega = m_read_data->coordinate_omega;
}

void VUnirobotAdapter::sleep(bool state) noexcept
{
    if(state) {
        m_write_data->watchdogTimer = SLEEP_UNIROBOT;
    }
    else {
        m_write_data->watchdogTimer = WATCH_UNIROBOT;
    }
}

void VUnirobotAdapter::setMotorSpeed(const vunirobot::task& task) noexcept
{
    register_type id = 0;
    int16_t value = 0;
    auto endIt = task.end();
    for(auto it = task.begin(); it != endIt; ++it) {
        id = it->first;
        value = it->second;

        assert(id >= 0 && id <= MOTOR_NUM);
        assert(value >= MOTOR_SPEED_MIN && value <= MOTOR_SPEED_MAX);

        memcpy(&m_write_data->motorInput[id], &value, sizeof(register_type));
    }
}

void VUnirobotAdapter::setMotionType(const vunirobot::task& task) noexcept
{
    register_type id = 0;
    int16_t value = 0;
    auto endIt = task.end();
    for(auto it = task.begin(); it != endIt; ++it) {
        id = it->first;
        value = it->second;

        assert(id >= 0 && id <= MOTOR_NUM);
        assert(value >= SPEED_MODE && value <= APOSITION);

        m_write_data->motionType[id] = value;;
    }
}

void VUnirobotAdapter::setDCMotorParameters(const vunirobot::dc_motor_parameters_task& task) noexcept
{
    register_type id = 0;
    DCMotorParameters value;
    auto endIt = task.end();
    for(auto it = task.begin(); it != endIt; ++it) {
        id = it->first;
        value = it->second;

        assert(id >= 0 && id <= MOTOR_NUM);

        m_write_data->motorParameters[id].proportional = value.proportional;
        m_write_data->motorParameters[id].integrated = value.integrated;
        m_write_data->motorParameters[id].differential = value.differential;
        m_write_data->motorParameters[id].saturation = value.saturation;
        m_write_data->motorParameters[id].saturationDifferential = value.saturationDifferential;
        m_write_data->motorParameters[id].saturationIntegrated = value.saturationIntegrated;
        m_write_data->motorParameters[id].saturationProportional = value.saturationProportional;
        m_write_data->motorParameters[id].sampling = value.sampling;
    }
}

void VUnirobotAdapter::setMotorDirection(const vunirobot::task& task) noexcept
{
    register_type id = 0;
    int16_t value = 0;
    auto endIt = task.end();
    for(auto it = task.begin(); it != endIt; ++it) {
        id = it->first;
        value = it->second;

        assert(id >= 0 && id <= MOTOR_NUM);
        assert(value == DIRECT || value == REVERSE);

        memcpy(&m_write_data->motorDirection[id], &value, sizeof(register_type));
    }
}

void VUnirobotAdapter::setSensorTypes(const vunirobot::task& task) noexcept
{
    register_type id;
    register_type value;
    auto endIt = task.end();
    for(auto it = task.begin(); it != endIt; ++it) {
        id = it->first;
        value = it->second;

        assert(id >= 0 && id <= vunirobot::SENSORS_NUM);
        assert((value > IR_SENSOR_TYPES_NULL && value < IR_SENSOR_TYPES_COUNT) || (value > US_SENSOR_TYPES_NULL && value << US_SENSOR_TYPES_COUNT));

        m_write_data->sensorType[id] = value;
    }
}

void VUnirobotAdapter::setRobotParameters(const vunirobot::RobotParameters& param) noexcept
{
    m_write_data->distanceBetweenWheels = param.distanceBetweenWheels;
    m_write_data->wheelRadius = param.wheelRadius;
    m_write_data->encoderTicksPerRotation = param.encoderTicksPerRotation;
    m_write_data->reductionRatio = param.reductionRatio;
}

bool VUnirobotAdapter::saveRobotConfig() noexcept
{
    if(!VModbusRTUMaster::isConnected()) {
        return false;
    }

    try {
        uint16_t writeValue = CONFIG_KEY_WRITE;
        bool write = VModbusRTUMaster::writeRegisters(CONFIG_KEY_REGISTER, 1, &writeValue);
        V_UNUSED(write);
        uint16_t readValue = 0;
        bool read = VModbusRTUMaster::readRegisters(CONFIG_KEY_REGISTER, 1, &readValue);
        V_UNUSED(read);
        bool result = (writeValue == readValue);
        return result;
    }
    catch(...) {
    }
    return false;
}

bool VUnirobotAdapter::resetMagneticTrigger() noexcept
{
    if(!VModbusRTUMaster::isConnected()) {
        return false;
    }

    try {
        uint16_t writeValue = RESET_MAGNETIC_TRIGGER_KEY;
        return VModbusRTUMaster::writeRegisters(RESET_MAGNETIC_TRIGGER, 1, &writeValue);
    }
    catch(...) {
    }
    return false;
}
