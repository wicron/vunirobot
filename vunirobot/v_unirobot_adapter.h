/*
 *  Copyright (c) 2012 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef _V_UNIROBOT_ADAPTER_H_
#define _V_UNIROBOT_ADAPTER_H_

#include <memory>

#include <vcore/hal/v_modbus_rtu_master.h>
#include <vunirobot/v_unirobot_global.h>

namespace vunirobot {
struct ReadData;
struct WriteData;
}

namespace vunirobot {

class VUNIROBOT_LIBRARY_EXPORT VUnirobotAdapter : public vcore::VModbusRTUMaster
{
public:
    enum { SERVO_POSITION_MIN = 0 };
    enum { SERVO_POSITION_MAX = 170 };

    static const int MOTOR_SPEED_MIN;
    static const int MOTOR_SPEED_MAX;

    enum ir_sensor_types {
        IR_SENSOR_TYPES_NULL = 0,
        GP2D120              = 1,
        GP2D12               = 2,
        GP2Y0A02YK0F         = 3,
        GP2Y0A21YKOF         = 4,
        Z2                   = 5,
        IR_SENSOR_TYPES_COUNT
    };

    enum us_sensor_type {
        US_SENSOR_TYPES_NULL = 0,
        HRLV                 = 1,
        LV                   = 2,
        US_SENSOR_TYPES_COUNT
    };

    enum motor_mode {
        SPEED_MODE    = 0,
        POSITION_MODE = 1,
        POSITION_WVC  = 2,
        ASPEED        = 3,
        APOSITION     = 4
    };

    enum direction {
        REVERSE = -1,
        DIRECT  =  1
    };

    enum READ_DATA_REGISTERS { READ_START_REGISTER = 1100,
                               READ_END_REGISTER   = 1157,
                               READ_DATA_LENGHT = READ_END_REGISTER - READ_START_REGISTER + 1 };

    enum WRITE_DATA_REGISTERS { WRITE_START_REGISTERS = 1169,
                                WRITE_END_REGISTERS   = 1240,
                                WRITE_DATA_LENGHT = WRITE_END_REGISTERS - WRITE_START_REGISTERS + 1 };

public:
    VUnirobotAdapter( ) noexcept;
    ~VUnirobotAdapter( ) override;

    bool connect(const std::string& portName) noexcept;

    const std::string& getPortName() const noexcept;

    bool readData();
    bool writeData();

    void getData(vunirobot::ReadData& data) const noexcept;

    int getBoardPower() const noexcept;
    int getMaxCapacity() const noexcept;
    bool isCharging() const noexcept;
    int getRemainingCapacity() const noexcept;
    void getSenorsValue(vunirobot::sensors_data& data) const noexcept;
    void getMotorSpeed(vunirobot::motor_speed& data) const noexcept;
    void getMagnet(vunirobot::magnet_data& data) const noexcept;
    void getAxel(vunirobot::axel_data& data) const noexcept;
    bool getMagnetState() const noexcept;
    void getRobotParameters(vunirobot::RobotParameters& param) const noexcept;
    void getRobotCoordinate(vunirobot::RobotCoordinate& coord) const noexcept;

    void sleep(bool state) noexcept;
    void setMotorSpeed(const vunirobot::task& task) noexcept;
    void setMotorDirection(const vunirobot::task& task) noexcept;
    void setMotionType(const vunirobot::task& task) noexcept;
    void setDCMotorParameters(const vunirobot::dc_motor_parameters_task& task) noexcept;
    void setSensorTypes(const vunirobot::task& task) noexcept;
    void setRobotParameters(const vunirobot::RobotParameters& param) noexcept;
    bool resetMagneticTrigger() noexcept;
    bool saveRobotConfig() noexcept;

private:
    enum { BOARD_ID = 0x01  };
    enum { BAUDRATE = 38400 };
    enum { DATA_BIT = 8     };
    enum { STOP_DIR = 1     };
    enum { TIMEOUT = 100000 };

    static const char PARITY;

    std::shared_ptr<vunirobot::ReadData>  m_read_data;
    std::shared_ptr<vunirobot::WriteData> m_write_data;

    std::string m_portName;
};

} // vunirobot

#endif // _V_UNIROBOT_ADAPTER_H_
