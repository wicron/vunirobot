/*
 *  Copyright (c) 2012 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef _V_UNIROBOT_GLOBAL_H_
#define _V_UNIROBOT_GLOBAL_H_

// ------------------------------------------------------------------- PLATFORM
#include <vcore/platform.h>
// ------------------------------------------------------------------- INCLUDES
#include <map>
#include <vector>

#include <stdint.h>
#include <cstring>

#include <vcore/types.h>
#include <vcore/v_core_global.h>
// ------------------------------------------------------------------- SYNOPSIS
// ----------------------------------------------------------------------------

#ifdef VUNIROBOT
#   define VUNIROBOT_LIBRARY_EXPORT V_DECL_EXPORT
#else
#   define VUNIROBOT_LIBRARY_EXPORT V_DECL_IMPORT
#endif // VUNIROBOT

// ----------------------------------------------------------------------------

namespace vunirobot {

struct DCMotorParameters {
    int proportional;
    int integrated;
    int differential;
    int saturation;
    int saturationProportional;
    int saturationIntegrated;
    int saturationDifferential;
    int sampling;
};

struct RobotParameters {
    int distanceBetweenWheels;
    int wheelRadius;
    int encoderTicksPerRotation;
    int reductionRatio;
};

struct RobotCoordinate {
    int coordinate_x;
    int coordinate_y;
    int coordinate_angle;
    int coordinate_omega;
};

typedef std::vector<vreal> sensors_data;
typedef std::vector<int16_t>   motor_speed;
typedef std::vector<int16_t>   magnet_data;
typedef std::vector<int16_t>   axel_data;
typedef std::map<int, int> task;
typedef std::map<int, DCMotorParameters> dc_motor_parameters_task;

typedef uint16_t register_type;
typedef int16_t  sign_register_type;

enum { DEFAULT_VALUE = 0 };

enum { SERVO_NUM   = 8 };
enum { SENSORS_NUM = 20 };
enum { IR_SENSOR_NUM = 12 };
enum { US_SENSOR_NUM = 8 };
enum { MOTOR_NUM   = 4 };

PACK (
struct ReadData
{
    ReadData( ) noexcept {
        size_t i;
        for( i = 0; i < SENSORS_NUM; ++i ) {
            sensorState[i] = DEFAULT_VALUE;
        }
        for( i = 0; i < MOTOR_NUM; ++i ) {
            motorCurrentSpeed[i] = DEFAULT_VALUE;
            motorCurrentPos[i] = DEFAULT_VALUE;
            encoder[i] = DEFAULT_VALUE;
            errorPid[i] = DEFAULT_VALUE;
        }
    }

    register_type boardState = DEFAULT_VALUE;
    register_type boardPower = DEFAULT_VALUE;
    register_type systemTimer = DEFAULT_VALUE;
    register_type sensorState[SENSORS_NUM];
    sign_register_type motorCurrentSpeed[MOTOR_NUM];
    sign_register_type motorCurrentPos[MOTOR_NUM];
    sign_register_type encoder[MOTOR_NUM];

    register_type trash0;

    sign_register_type errorPid[MOTOR_NUM];

    sign_register_type magnetX = DEFAULT_VALUE;
    sign_register_type magnetY = DEFAULT_VALUE;
    sign_register_type magnetZ = DEFAULT_VALUE;
    sign_register_type axelX = DEFAULT_VALUE;
    sign_register_type axelY = DEFAULT_VALUE;
    sign_register_type axelZ = DEFAULT_VALUE;
    sign_register_type magnetState = DEFAULT_VALUE;

    register_type trash1;
    register_type resetRegister = DEFAULT_VALUE;
    register_type trash2;

    register_type distanceBetweenWheels = DEFAULT_VALUE;
    register_type wheelRadius = DEFAULT_VALUE;
    register_type encoderTicksPerRotation = DEFAULT_VALUE;
    register_type reductionRatio = DEFAULT_VALUE;

    sign_register_type coordinate_x = DEFAULT_VALUE;
    sign_register_type coordinate_y = DEFAULT_VALUE;
    sign_register_type coordinate_angle = DEFAULT_VALUE;
    sign_register_type coordinate_omega = DEFAULT_VALUE;
});

PACK (
struct WriteData
{
    PACK (
    struct MotorParams
    {
        register_type proportional = DEFAULT_VALUE;
        register_type integrated = DEFAULT_VALUE;
        register_type differential = DEFAULT_VALUE;
        register_type saturation = DEFAULT_VALUE;
        register_type saturationProportional = DEFAULT_VALUE;
        register_type saturationIntegrated = DEFAULT_VALUE;
        register_type saturationDifferential = DEFAULT_VALUE;
        register_type sampling = DEFAULT_VALUE;
    });

    WriteData() noexcept {
        size_t i;
        for( i = 0; i < SENSORS_NUM; ++i ) {
            sensorType[i] = DEFAULT_VALUE;
        }
        for( i = 0; i < MOTOR_NUM; ++i ) {
            motionType[i] = DEFAULT_VALUE;
            motorDirection[i] = DEFAULT_VALUE;
            motorInput[i] = DEFAULT_VALUE;
        }
        memset( trash, 0, 3 * sizeof( register_type ) );
    }

    register_type watchdogTimer = DEFAULT_VALUE;
    register_type sensorType[SENSORS_NUM];
    register_type motionType[MOTOR_NUM];
    register_type motorDirection[MOTOR_NUM];
    register_type motorInput[MOTOR_NUM];
    register_type trash[3];
    MotorParams   motorParameters[MOTOR_NUM];
    register_type distanceBetweenWheels = DEFAULT_VALUE;
    register_type wheelRadius = DEFAULT_VALUE;
    register_type encoderTicksPerRotation = DEFAULT_VALUE;
    register_type reductionRatio = DEFAULT_VALUE;
});

} // vunirobot

#endif // _V_UNIROBOT_GLOBAL_H_
