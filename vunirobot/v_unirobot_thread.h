/*
 *  Copyright (c) 2012 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef _V_UNIROBOT_THREAD_H_
#define _V_UNIROBOT_THREAD_H_

// ------------------------------------------------------------------- INCLUDES
#include <vcore/v_core_global.h>
#include <vcore/cs/v_device_thread.h>
// ------------------------------------------------------------------- SYNOPSIS
#include <vunirobot/v_unirobot_global.h>
#include <vunirobot/v_unirobot_adapter.h>
// ----------------------------------------------------------------------------

namespace vunirobot {

class VUNIROBOT_LIBRARY_EXPORT VUnirobotThread : public vcore::VDeviceThread<VUnirobotAdapter, ReadData>
{
public:
    bool getBoardPower(int& power) noexcept;
    bool getMaxCapacity(int& capacity) noexcept;
    bool isCharging(bool& state) noexcept;
    bool getRemainingCapacity(int& capacity) noexcept;
    bool getSenorsValue(vunirobot::sensors_data& data) noexcept;
    bool getMotorSpeed(vunirobot::motor_speed& data) noexcept;
    bool getMagnet(vunirobot::magnet_data& data) noexcept;
    bool getAxel(vunirobot::axel_data& data) noexcept;
    bool getMagnetState(bool& state) noexcept;
    bool getRobotParameters(vunirobot::RobotParameters& param) noexcept;
    bool getRobotCoordinate(vunirobot::RobotCoordinate& coord) noexcept;

    bool sleep(bool state) noexcept;
    bool setMotionType(const vunirobot::task& task) noexcept;
    bool setMotorSpeed(const vunirobot::task& task) noexcept;
    bool setMotorDirection(const vunirobot::task& task) noexcept;
    bool setDCMotorParameters(const vunirobot::dc_motor_parameters_task& task) noexcept;
    bool setSensorTypes(const vunirobot::task& task) noexcept;
    bool setRobotParameters(const vunirobot::RobotParameters& param) noexcept;
    bool resetMagneticTrigger() noexcept;
    bool saveRobotConfig() noexcept;
};

} // vunirobot

#endif // _V_UNIROBOT_THREAD_H_
