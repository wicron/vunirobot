/*
 *  Copyright (c) 2012 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "v_unirobot_thread.h"

#include <cassert>
#include <fstream>
#include <iostream>
#include <mutex>

using namespace vunirobot;

bool VUnirobotThread::getBoardPower(int& power) noexcept
{
    std::unique_lock<boost::mutex> locker(*getRMutex());
    power = getBuffer()->boardPower;
    return working();
}

bool VUnirobotThread::isCharging(bool& state) noexcept
{
    std::unique_lock<boost::mutex> locker(*getRMutex());
    state = getBuffer()->boardState;
    return working();
}

bool VUnirobotThread::getSenorsValue(vunirobot::sensors_data& data) noexcept
{
    data.resize(vunirobot::SENSORS_NUM);
    auto buffer = getBuffer();
    std::unique_lock<boost::mutex> locker(*getRMutex());
    for(size_t i = 0; i < vunirobot::SENSORS_NUM; ++i) {
        data[i] = buffer->sensorState[i];
    }
    return working();
}

bool VUnirobotThread::getMotorSpeed(vunirobot::motor_speed& data) noexcept
{
    data.resize(vunirobot::MOTOR_NUM);
    auto buffer = getBuffer();
    std::unique_lock<boost::mutex> locker(*getRMutex());
    for(size_t i = 0; i < vunirobot::MOTOR_NUM; ++i) {
        data[i] = (int16_t)buffer->encoder[i];
    }
    return working();
}

bool VUnirobotThread::getMagnet(vunirobot::magnet_data& data) noexcept
{
    data.resize(3);
    auto buffer = getBuffer();
    std::unique_lock<boost::mutex> locker(*getRMutex());
    data[0] = buffer->magnetX;
    data[1] = buffer->magnetY;
    data[2] = buffer->magnetZ;
    return working();
}

bool VUnirobotThread::getAxel(vunirobot::axel_data& data) noexcept
{
    auto buffer = getBuffer();
    data.resize(3);
    std::unique_lock<boost::mutex> locker(*getRMutex());
    data[0] = buffer->axelX;
    data[1] = buffer->axelY;
    data[2] = buffer->axelZ;
    return working();
}

bool VUnirobotThread::getMagnetState(bool& state) noexcept
{
    std::unique_lock<boost::mutex> locker(*getRMutex());
    state = getBuffer()->magnetState > 0;
    return working();
}

bool VUnirobotThread::getRobotParameters(vunirobot::RobotParameters& param) noexcept
{
    auto buffer = getBuffer();
    std::unique_lock<boost::mutex> locker(*getRMutex());
    param.distanceBetweenWheels = buffer->distanceBetweenWheels;
    param.wheelRadius = buffer->wheelRadius;
    param.encoderTicksPerRotation = buffer->encoderTicksPerRotation;
    param.reductionRatio = buffer->reductionRatio;
    return working();
}

bool VUnirobotThread::getRobotCoordinate(vunirobot::RobotCoordinate& coord) noexcept
{
    auto buffer = getBuffer();
    std::unique_lock<boost::mutex> locker(*getRMutex());
    coord.coordinate_x = buffer->coordinate_x;
    coord.coordinate_y = buffer->coordinate_y;
    coord.coordinate_angle = buffer->coordinate_angle;
    coord.coordinate_omega = buffer->coordinate_omega;
    return working();
}

bool VUnirobotThread::sleep(bool state) noexcept
{
    {
        std::unique_lock<boost::mutex> locker(*getWMutex());
        getAdapter()->sleep(state);
        write();
    }
    notifyDataChanged( );
    return working();
}

bool VUnirobotThread::setMotionType(const vunirobot::task& task) noexcept
{
    {
        std::unique_lock<boost::mutex> locker(*getWMutex());
        getAdapter()->setMotionType(task);
        write();
    }
    notifyDataChanged( );
    return working();
}

bool VUnirobotThread::setMotorSpeed(const vunirobot::task& task) noexcept
{
    {
        std::unique_lock<boost::mutex> locker(*getWMutex());
        getAdapter()->setMotorSpeed(task);
        write();
    }
    notifyDataChanged( );
    return working();
}

bool VUnirobotThread::setMotorDirection(const vunirobot::task& task) noexcept
{
    {
        std::unique_lock<boost::mutex> locker(*getWMutex());
        getAdapter()->setMotorDirection(task);
        write();
    }
    notifyDataChanged( );
    return working();
}

bool VUnirobotThread::setDCMotorParameters(const vunirobot::dc_motor_parameters_task& task) noexcept
{
    {
        std::unique_lock<boost::mutex> locker(*getWMutex());
        getAdapter()->setDCMotorParameters(task);
        write();
    }
    notifyDataChanged( );
    return working();
}

bool VUnirobotThread::setSensorTypes(const vunirobot::task& task) noexcept
{
    {
        std::unique_lock<boost::mutex> locker(*getWMutex());
        getAdapter()->setSensorTypes(task);
        write();
    }
    notifyDataChanged( );
    return working();
}

bool VUnirobotThread::setRobotParameters(const vunirobot::RobotParameters& param) noexcept
{
    {
        std::unique_lock<boost::mutex> locker(*getWMutex());
        getAdapter()->setRobotParameters(param);
        write();
    }
    notifyDataChanged( );
    return working();
}

bool VUnirobotThread::resetMagneticTrigger() noexcept
{
    std::unique_lock<boost::mutex> locker(*getWMutex());
    return getAdapter()->resetMagneticTrigger();
}

bool VUnirobotThread::saveRobotConfig() noexcept
{
    std::unique_lock<boost::mutex> locker(*getWMutex());
    return getAdapter()->saveRobotConfig();
}
