#==============================================================================
# Copyright (c) 2011-2012 Evgeny Proydakov <lord.tiran@gmail.com>
#==============================================================================

MACRO(ADD_ANDROID_TEST args)

MESSAGE(STATUS "create  ${ARGV0}")

INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR})
INCLUDE_DIRECTORIES(${RDO_BASE_DIRECTORY})

ADD_EXECUTABLE(${ARGV0} ${ARGN})
SET_TARGET_PROPERTIES(${ARGV0} PROPERTIES LINKER_LANGUAGE CXX)
ADD_TEST(NAME ${ARGV0} COMMAND ${EXECUTABLE_OUTPUT_PATH}/${ARGV0})

TARGET_LINK_LIBRARIES(${ARGV0} -lgnustl_shared -lcrystax_shared)

SOURCE_GROUP(".test" FILES
    ${ARGN})

ENDMACRO(ADD_ANDROID_TEST)
