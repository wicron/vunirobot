#==============================================================================
# Copyright (c) 2012 Evgeny Proydakov <lord.tiran@gmail.com>
#
# This file is a modification of file that was created automaticali by a system
# of buildroot "toolchainfile.cmake".
#
#==============================================================================

SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_PROCESSOR "armv6")

SET(CMAKE_C_COMPILER ${BSQUASK_SDK}/host/usr/bin/arm-raspberrypi-linux-gnueabi-gcc)
SET(CMAKE_CXX_COMPILER ${BSQUASK_SDK}/host/usr/bin/arm-raspberrypi-linux-gnueabi-g++)
SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}  -pipe -mfloat-abi=hard -mfpu=vfp -mtune=arm1176jzf-s -march=armv6zk -O2  -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64" CACHE STRING "Buildroot CFLAGS" FORCE)
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  -pipe -mfloat-abi=hard -mfpu=vfp -mtune=arm1176jzf-s -march=armv6zk -O2  -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64" CACHE STRING "Buildroot CXXFLAGS" FORCE)
SET(CMAKE_INSTALL_SO_NO_EXE 0)
SET(CMAKE_PROGRAM_PATH "${BSQUASK_SDK}/host/usr/bin")
SET(CMAKE_FIND_ROOT_PATH "${BSQUASK_SDK}/host/usr/arm-unknown-linux-gnueabi/sysroot")
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
SET(ENV{PKG_CONFIG_SYSROOT_DIR} "${BSQUASK_SDK}/host/usr/arm-unknown-linux-gnueabi/sysroot")
