#==============================================================================
# Copyright (c) 2011-2014 Evgeny Proydakov <lord.tiran@gmail.com>
#
#  VCORE_FOUND - system has VCORE
#  VCORE_INCLUDE_DIRS - the VCORE include directory
#  VCORE_LIBRARIES - Link these to use VCORE
#
#==============================================================================

IF(VCORE_INCLUDE_DIRS AND VCORE_LIBRARIES)

    SET(VCORE_FOUND TRUE)
    MESSAGE(STATUS "Found vcore: ${VCORE_LIBRARIES}")

ELSE()

    FIND_PATH(VCORE_INCLUDE_DIRS vcore
        PATHS
        /usr/local/include
        $ENV{VCORE_DIRECTORY}/include
        )

    FIND_LIBRARY(VCORE_LIBRARY_DEBUG
        NAMES vcored
        PATHS
        /usr/local/lib
        $ENV{VCORE_DIRECTORY}/lib
        )

    FIND_LIBRARY(VCORE_LIBRARY_RELEASE
        NAMES vcore
        PATHS
        /usr/local/lib
        $ENV{VCORE_DIRECTORY}/lib
        )

    GET_FILENAME_COMPONENT(VCORE_LIBRARY_DEBUG_DIR ${VCORE_LIBRARY_DEBUG} PATH)
    GET_FILENAME_COMPONENT(VCORE_LIBRARY_RELEASE_DIR ${VCORE_LIBRARY_RELEASE} PATH)

    SET(VCORE_FOUND TRUE)

    IF(NOT VCORE_INCLUDE_DIRS)
        SET(VCORE_FOUND FALSE)
    ENDIF(NOT VCORE_INCLUDE_DIRS)

    IF(NOT VCORE_LIBRARY_DEBUG  AND  NOT VCORE_LIBRARY_RELEASE)
        SET(VCORE_FOUND FALSE)
    ELSEIF(NOT VCORE_LIBRARY_DEBUG)
        SET(VCORE_LIBRARY_DEBUG ${VCORE_LIBRARY_RELEASE})
    ELSEIF(NOT VCORE_LIBRARY_RELEASE)
        SET(VCORE_LIBRARY_RELEASE ${VCORE_LIBRARY_DEBUG})
    ENDIF()

    SET(VCORE_LIBRARIES "debug" ${VCORE_LIBRARY_DEBUG} "optimized" ${VCORE_LIBRARY_RELEASE})
    SET(VCORE_LIBRARY_DIRS ${VCORE_LIBRARY_DEBUG_DIR} ${VCORE_LIBRARY_RELEASE_DIR} CACHE FILEPATH "vcore library directories")

    MARK_AS_ADVANCED(VCORE_INCLUDE_DIRS VCORE_LIBRARIES VCORE_LIBRARY_DIRS)

ENDIF()

#==============================================================================
