/*
 *  Copyright (c) 2012 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// ------------------------------------------------------------------- PLATFORM
#include <vcore/platform.h>
// ------------------------------------------------------------------- INCLUDES
#include <iostream>

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>

#define BOOST_TEST_MODULE TEST_UNIROBOT_ADAPTER
#include <boost/test/included/unit_test.hpp>

#ifdef OS_WINDOWS
#   include <vcore/hal/v_serial_port_manager.h>
#endif // OS_WINDOWS

#include <vcore/utils/v_benchmark.h>
// ------------------------------------------------------------------- SYNOPSIS
#include <vunirobot/v_unirobot_adapter.h>
// ----------------------------------------------------------------------------

#if defined ( OS_WINDOWS )
#   define PORT_NAME "COM6"
#elif defined ( OS_UNIX )
#   define PORT_NAME "/dev/unirobot"
#endif

const int RECONNECT_ITER  = 30;
const int CYCLE_TIMEOUT   = 100;
const int SET_POS_TIMEOUT = 750;
const int CYCLE_ITER      = 1;

void port_not_found()
{
    std::cerr << "port : " << PORT_NAME << " not found" << std::endl;
    exit(0);
}

using namespace vunirobot;

class VModbusTestReader : public VUnirobotAdapter
{
public:
    VModbusTestReader() {}
    virtual ~VModbusTestReader() {}

    bool readSome()
    {
        return VUnirobotAdapter::readRegisters(1205, 8, reinterpret_cast<uint16_t*>(&params));
    }

    bool readOne(int adress, int& data)
    {
        uint16_t idata = 0;
        bool res = VUnirobotAdapter::readRegisters(adress, 1, &idata);
        data = idata;
        return res;
    }

    WriteData::MotorParams params;
};

boost::shared_ptr<VUnirobotAdapter> g_p_adapter;

BOOST_AUTO_TEST_SUITE(TEST_UNIROBOT_ADAPTER)

BOOST_AUTO_TEST_CASE(init)
{
#ifdef OS_UNIX
    bool check = boost::filesystem::exists(PORT_NAME);
    if(!check) {
        port_not_found();
    }
#endif // OS_UNIX

#ifdef OS_WINDOWS
    std::list<std::string> ports;
    vcore::VSerialPortManager::getListPorts(ports);
    auto it = std::find(ports.begin(), ports.end(), PORT_NAME);
    if(it == ports.end()) {
        port_not_found();
    }
#endif // OS_WINDOWS

    g_p_adapter = boost::shared_ptr<VUnirobotAdapter>(new VUnirobotAdapter);
    if(!g_p_adapter->connect(PORT_NAME)) {
        std::cout << "CONNECT ERROR" << std::endl;
        exit(1);
    }
    std::cout << "CONNECT OK" << std::endl;
}

BOOST_AUTO_TEST_CASE(reconnect)
{
    for(int i = 0; i < RECONNECT_ITER; ++i){
        BOOST_CHECK(g_p_adapter->reconnect());
    }
}

BOOST_AUTO_TEST_CASE(set_sensor_type)
{
    vunirobot::task u_task;

    for(size_t i = 0; i < vunirobot::SENSORS_NUM; ++i) {
        u_task.insert(vunirobot::task::value_type(i, VUnirobotAdapter::GP2D12));
    }

    std::cout << "SET SENSORS TYPE" << std::endl;
    g_p_adapter->setSensorTypes(u_task);
    BOOST_CHECK(g_p_adapter->writeData());
}

BOOST_AUTO_TEST_CASE(set_engine_params)
{
    for(int i = 0; i < CYCLE_ITER; ++i) {
        DCMotorParameters params;

        params.proportional = 30;
        params.integrated = 100;
        params.differential = 10;
        params.saturation = 32760;

        params.saturationProportional = 32760;
        params.saturationIntegrated = 32760;
        params.saturationDifferential = 32760;
        params.sampling = 50;

        dc_motor_parameters_task task;
        task.insert(std::make_pair(0, params));
        g_p_adapter->setDCMotorParameters(task);

        BOOST_CHECK(g_p_adapter->writeData());

        g_p_adapter->disconnect();

        VModbusTestReader reader;
        BOOST_CHECK(reader.connect(PORT_NAME));
        BOOST_CHECK(reader.readSome());

        BOOST_CHECK_EQUAL(params.differential, reader.params.differential);
        BOOST_CHECK_EQUAL(params.integrated, reader.params.integrated);
        BOOST_CHECK_EQUAL(params.proportional, reader.params.proportional);
        BOOST_CHECK_EQUAL(params.saturation, reader.params.saturation);

        BOOST_CHECK_EQUAL(params.sampling, reader.params.sampling);
        BOOST_CHECK_EQUAL(params.saturationDifferential, reader.params.saturationDifferential);
        BOOST_CHECK_EQUAL(params.saturationIntegrated, reader.params.saturationIntegrated);
        BOOST_CHECK_EQUAL(params.saturationProportional, reader.params.saturationProportional);

        for(int adress = 1205; adress <= 1236; adress++) {
            int data = 0;
            reader.readOne(adress, data);
            std::cout << adress << " - " << data << std::endl;
        }

        reader.disconnect();
    }
}

BOOST_AUTO_TEST_CASE(get_board_state)
{
    g_p_adapter->connect(PORT_NAME);

    std::cout << "Now the state board will be displayed:" << std::endl;

    for(int i = 0; i < CYCLE_ITER; ++i) {
        BOOST_CHECK(g_p_adapter->readData());
        std::cout << "CHARGING STATE: " << g_p_adapter->isCharging() << std::endl;
        boost::this_thread::sleep(boost::posix_time::milliseconds(CYCLE_TIMEOUT));
    }
}

BOOST_AUTO_TEST_CASE(get_sensors_state)
{
    for(int i = 0; i < CYCLE_ITER; ++i) {
        BOOST_CHECK(g_p_adapter->readData());
        vunirobot::sensors_data data(vunirobot::SENSORS_NUM);
        g_p_adapter->getSenorsValue(data);
        std::cout << "SENSORS STATE:" << std::endl;
        for(size_t j = 0; j < vunirobot::SENSORS_NUM; ++j) {
            std::cout << data[j] << std::endl;
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(CYCLE_TIMEOUT));
    }
}

BOOST_AUTO_TEST_CASE(benchmark_reconnect)
{
    vcore::VBenchmark::run(boost::bind(&VUnirobotAdapter::reconnect, g_p_adapter), "VUnirobotAdapter::reconnect");
}

BOOST_AUTO_TEST_CASE(benchmark_read)
{
    vcore::VBenchmark::run(boost::bind(&VUnirobotAdapter::readData, g_p_adapter), "VUnirobotAdapter::readData");
}

BOOST_AUTO_TEST_CASE(benchmark_write)
{
    vcore::VBenchmark::run(boost::bind(&VUnirobotAdapter::writeData, g_p_adapter), "VUnirobotAdapter::writeData");
}

BOOST_AUTO_TEST_CASE(cleanup)
{
    g_p_adapter->disconnect();
}

BOOST_AUTO_TEST_SUITE_END() // TEST_UNIROBOT_ADAPTER
