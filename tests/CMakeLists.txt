#==============================================================================
# Copyright (c) 2012 Evgeny Proydakov <lord.tiran@gmail.com>
#==============================================================================

MESSAGE(STATUS "CREATE TEST\n")

ADD_SUBDIRECTORY(test_unirobot_read)
ADD_SUBDIRECTORY(test_unirobot_thread)
ADD_SUBDIRECTORY(test_unirobot_adapter)
ADD_SUBDIRECTORY(test_unirobot_odometry)
