#==============================================================================
# Copyright (c) 2013 Evgeny Proydakov lord.tiran@gmail.com
#==============================================================================

FIND_PACKAGE(Boost COMPONENTS filesystem REQUIRED)
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})

ADD_BOOST_TEST(test_unirobot_odometry main.cpp)

ADD_DEPENDENCIES(test_unirobot_odometry vunirobot)
TARGET_LINK_LIBRARIES(test_unirobot_odometry vunirobot)
TARGET_LINK_LIBRARIES(test_unirobot_odometry ${Boost_FILESYSTEM_LIBRARY})
