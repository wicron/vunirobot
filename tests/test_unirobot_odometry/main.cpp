/*
 *  Copyright (c) 2013 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// ------------------------------------------------------------------- PLATFORM
#include <vcore/platform.h>
// ------------------------------------------------------------------- INCLUDES
#include <iostream>

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>

#define BOOST_TEST_MODULE TEST_UNIROBOT_ADAPTER
#include <boost/test/included/unit_test.hpp>

#ifdef OS_WINDOWS
#   include <vcore/hal/v_serial_port_manager.h>
#endif // OS_WINDOWS
// ------------------------------------------------------------------- SYNOPSIS
#include <vunirobot/v_unirobot_thread.h>
// ----------------------------------------------------------------------------

#if defined ( OS_WINDOWS )
#   define PORT_NAME "COM6"
#elif defined ( OS_UNIX )
#   define PORT_NAME "/dev/unirobot"
#endif

void port_not_found()
{
    std::cerr << "port : " << PORT_NAME << " not found" << std::endl;
    exit(0);
}

using namespace vunirobot;

boost::shared_ptr<VUnirobotThread> g_p_thread;

BOOST_AUTO_TEST_SUITE(TEST_UNIROBOT_ODOMETRY)

BOOST_AUTO_TEST_CASE(init)
{
#ifdef OS_UNIX
    bool check = boost::filesystem::exists(PORT_NAME);
    if(!check) {
        port_not_found();
    }
#endif // OS_UNIX

#ifdef OS_WINDOWS
    std::list<std::string> ports;
    vcore::VSerialPortManager::getListPorts(ports);
    auto it = std::find(ports.begin(), ports.end(), PORT_NAME);
    if(it == ports.end()) {
        port_not_found();
    }
#endif // OS_WINDOWS

    g_p_thread = boost::shared_ptr<VUnirobotThread>(new VUnirobotThread);
    if(!g_p_thread->connectToPort(PORT_NAME)) {
        std::cout << "CONNECT ERROR" << std::endl;
        exit(1);
    }
    std::cout << "CONNECT OK" << std::endl;
}

BOOST_AUTO_TEST_CASE(stress)
{
    RobotCoordinate coordinate;
    for(size_t i = 0; i < 1000000; i++) {
        bool get = g_p_thread->getRobotCoordinate(coordinate);
        std::cout << "get: " << get << std::endl;
        std::cout << "x: " << coordinate.coordinate_x << std::endl;
        std::cout << "y: " << coordinate.coordinate_y << std::endl;
        std::cout << "angle: " << coordinate.coordinate_angle << std::endl;
        std::cout << std::endl;
    }
}

BOOST_AUTO_TEST_CASE(cleanup)
{
    g_p_thread->disconnectFromPort();
}

BOOST_AUTO_TEST_SUITE_END() // TEST_UNIROBOT_ADAPTER
