/*
 *  Copyright (c) 2012 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// ------------------------------------------------------------------- PLATFORM
#include <vcore/platform.h>
// ------------------------------------------------------------------- INCLUDES
#include <iostream>

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>

#define BOOST_TEST_MODULE TEST_UNIROBOT_THREAD
#include <boost/test/included/unit_test.hpp>

#ifdef OS_WINDOWS
#   include <vcore/hal/v_serial_port_manager.h>
#endif // OS_WINDOWS
// ------------------------------------------------------------------- SYNOPSIS
#include <vunirobot/v_unirobot_thread.h>
// ----------------------------------------------------------------------------

#if defined ( OS_WINDOWS )
#   define PORT_NAME "COM6"
#elif defined ( OS_UNIX )
#   define PORT_NAME "/dev/unirobot"
#endif

const int RECONNECT_ITER      = 500;
const int CYCLE_TIMEOUT       = 100;
const int SET_POS_TIMEOUT     = 750;
const int CYCLE_ITER          = 10;
const int STRESS_TEST_TIMEOUT = 3000; // ms

void port_not_found()
{
    std::cerr << "port : " << PORT_NAME << " not found" << std::endl;
    exit(0);
}

using namespace vunirobot;

boost::shared_ptr<VUnirobotThread> g_p_thread;

BOOST_AUTO_TEST_SUITE(TEST_UNIROBOT_THREAD)

BOOST_AUTO_TEST_CASE(init)
{
#ifdef OS_UNIX
    bool check = boost::filesystem::exists(PORT_NAME);
    if(!check) {
        port_not_found();
    }
#endif // OS_UNIX

#ifdef OS_WINDOWS
    std::list<std::string> ports;
    vcore::VSerialPortManager::getListPorts(ports);
    auto it = std::find(ports.begin(), ports.end(), PORT_NAME);
    if(it == ports.end()) {
        port_not_found();
    }
#endif // OS_WINDOWS

    g_p_thread = boost::shared_ptr<VUnirobotThread>(new VUnirobotThread);
    if(!g_p_thread->connectToPort(PORT_NAME)) {
        std::cout << "CONNECT ERROR" << std::endl;
        exit(1);
    }

    DCMotorParameters param;
    param.proportional = 30;
    param.integrated = 100;
    param.differential = 10;
    param.saturation = 32760;
    param.saturationProportional = 32760;
    param.saturationIntegrated = 32760;
    param.saturationDifferential = 32760;
    param.sampling = 50;

    vunirobot::dc_motor_parameters_task config_engine;
    for(size_t i = 0; i < vunirobot::MOTOR_NUM; i++) {
        config_engine.insert(std::make_pair(i, param));
    }
    g_p_thread->setDCMotorParameters(config_engine);

    std::cout << "settings" << std::endl;

    boost::this_thread::sleep(boost::posix_time::milliseconds(3500));
}

BOOST_AUTO_TEST_CASE(stability_connect)
{
    for(int i = 0; i < RECONNECT_ITER; ++i){
        BOOST_CHECK(g_p_thread->connectToPort(PORT_NAME));
    }
}

BOOST_AUTO_TEST_CASE(stability_disconnect)
{
    for(int i = 0; i < RECONNECT_ITER; ++i){
        g_p_thread->disconnectFromPort();
    }

    g_p_thread->connectToPort(PORT_NAME);
}

BOOST_AUTO_TEST_CASE(set_robot_params)
{
    for(int i = 0; i < 1; ++i) {
        std::cout << "UNIROBOT ROBOT PARAMS:" << std::endl;

        vunirobot::RobotParameters params;
        params.distanceBetweenWheels = 346;
        params.encoderTicksPerRotation = 8;
        params.reductionRatio = 139;
        params.wheelRadius = 101;

        std::cout << "set: " << g_p_thread->setRobotParameters(params) << std::endl;
        bool save = false;
        for(int j = 0; j < 10 && !save; j++) {
            save = g_p_thread->saveRobotConfig();
            std::cout << "save: " << save << std::endl;
        }

        boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
    }
}

BOOST_AUTO_TEST_CASE(get_robot_params)
{
    for(int i = 0; i < 10; ++i) {
        std::cout << "UNIROBOT ROBOT PARAMS:" << std::endl;

        vunirobot::RobotParameters params;
        std::cout << "read: " << g_p_thread->getRobotParameters(params) << " "
                  << "distanceBetweenWheels: "   << params.distanceBetweenWheels << " "
                  << "encoderTicksPerRotation: " << params.encoderTicksPerRotation << " "
                  << "reductionRatio: "          << params.reductionRatio << " "
                  << "wheelRadius: "             << params.wheelRadius << " "
                  << std::endl;

        boost::this_thread::sleep(boost::posix_time::milliseconds(CYCLE_TIMEOUT));
    }
}

BOOST_AUTO_TEST_CASE(get_coord)
{
    for(int i = 0; i < 10; ++i) {
        std::cout << "UNIROBOT ROBOT COORD:" << std::endl;

        vunirobot::RobotCoordinate coord;
        std::cout << "read: " << g_p_thread->getRobotCoordinate(coord) << " "
                  << "coordinate_x: "     << coord.coordinate_x << " "
                  << "coordinate_y: "    << coord.coordinate_y << " "
                  << "coordinate_angle: " << coord.coordinate_angle << " "
                  << "coordinate_omega: " << coord.coordinate_omega << " "
                  << std::endl;

        boost::this_thread::sleep(boost::posix_time::milliseconds(CYCLE_TIMEOUT));
    }
}

BOOST_AUTO_TEST_CASE(magnet)
{
    for(int i = 0; i < 500; ++i) {
        std::cout << "UNIROBOT MAGENT: ";

        bool state = false;
        bool read = g_p_thread->getMagnetState(state);
        std::cout << "read: " << read << " "
                  << "state: " << state << std::endl;

        if(read && state) {
            bool reset = g_p_thread->resetMagneticTrigger();
            std::cout << "reset: " << reset << std::endl;
        }

        boost::this_thread::sleep(boost::posix_time::milliseconds(CYCLE_TIMEOUT));
    }
}

BOOST_AUTO_TEST_CASE(cycle)
{
    bool board_state = false;

    for(int i = 0; i < CYCLE_ITER; ++i) {
        std::cout << "UNIROBOT DATA:" << std::endl;
        board_state = g_p_thread->isCharging(board_state);
        if(board_state) {
            std::cout << "CHARGING STATE: " << board_state << std::endl;
        }
        else {
            std::cout << "READ DATA ERROR" << std::endl;
        }

        std::cout << "READ DATA ITER : " << i << std::endl;

        vunirobot::sensors_data data(vunirobot::SENSORS_NUM);
        if(g_p_thread->getSenorsValue(data))
            for(size_t j = 0; j < vunirobot::SENSORS_NUM; ++j) {
                std::cout << "SENSOR " << j << " - " << data[j] << std::endl;
            }
        else {
            std::cout << "READ DATA ERROR" << std::endl;
        }

        boost::this_thread::sleep(boost::posix_time::milliseconds(CYCLE_TIMEOUT));

        std::cout << std::endl;
    }
}

BOOST_AUTO_TEST_CASE(direction)
{
    std::cout << "CONFIG" << std::endl;

    vunirobot::task task;

    for(int i = 0; i < CYCLE_ITER; ++i) {
        std::cout << "config iteration: " << i << std::endl;
        for(size_t i = 0; i < vunirobot::MOTOR_NUM; i++) {
            task.insert(std::make_pair(i, VUnirobotAdapter::REVERSE));
        }
        BOOST_CHECK(g_p_thread->setMotorDirection(task));
        task.clear();

        boost::this_thread::sleep(boost::posix_time::milliseconds(STRESS_TEST_TIMEOUT));

        for(size_t i = 0; i < vunirobot::MOTOR_NUM; i++) {
            task.insert(std::make_pair(i, VUnirobotAdapter::DIRECT));
        }
        BOOST_CHECK(g_p_thread->setMotorDirection(task));
        task.clear();

        boost::this_thread::sleep(boost::posix_time::milliseconds(STRESS_TEST_TIMEOUT));
    }
}

BOOST_AUTO_TEST_CASE(stress)
{
    std::cout << "STRESS TEST" << std::endl;

    vunirobot::task task;
    for(int i = 0; i < CYCLE_ITER; ++i) {
        std::cout << "stress iteration: " << i << std::endl;

        for(size_t j = 0; j < vunirobot::MOTOR_NUM; j++) {
            task.insert(std::make_pair(j, VUnirobotAdapter::MOTOR_SPEED_MAX));
        }
        BOOST_CHECK(g_p_thread->setMotorSpeed(task));
        task.clear();

        boost::this_thread::sleep(boost::posix_time::milliseconds(STRESS_TEST_TIMEOUT));

        for(size_t j = 0; j < vunirobot::MOTOR_NUM; j++) {
            task.insert(std::make_pair(j, VUnirobotAdapter::MOTOR_SPEED_MIN));
        }
        BOOST_CHECK(g_p_thread->setMotorSpeed(task));
        task.clear();

        boost::this_thread::sleep(boost::posix_time::milliseconds(STRESS_TEST_TIMEOUT));
    }

    for(size_t j = 0; j < vunirobot::MOTOR_NUM; j++) {
        task.insert(std::make_pair(j, 0));
    }
    BOOST_CHECK(g_p_thread->setMotorSpeed(task));
}

BOOST_AUTO_TEST_CASE(cleanup)
{
    g_p_thread->disconnectFromPort();
}

BOOST_AUTO_TEST_SUITE_END() // TEST_UNIROBOT_THREAD
