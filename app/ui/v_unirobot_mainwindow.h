/*
 *  Copyright (c) 2012 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef _V_UNIROBOT_MAINWINDOW_H_
#define _V_UNIROBOT_MAINWINDOW_H_

// ------------------------------------------------------------------- INCLUDES
#include <map>

#include <boost/shared_ptr.hpp>

#include <QMainWindow>
// ------------------------------------------------------------------- SYNOPSIS
// ----------------------------------------------------------------------------

namespace boost{
class thread;
}

class QSpinBox;
class QCheckBox;

namespace vunirobot {
class VUnirobotThread;
}

namespace Ui {
class VUnirobotMainWindow;
}

using namespace vunirobot;

class VUnirobotMainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit VUnirobotMainWindow(QWidget *parent = 0);
    virtual ~VUnirobotMainWindow();

private:
    void startUpdateUiThread();
    void updatedUiThreadFun(const boost::shared_ptr<VUnirobotThread>& thread, const bool& run);
    void stopUpdateUiThread();

    void startAwakingThread();
    void awakingThreadFun(const boost::shared_ptr<VUnirobotThread>& thread, const bool& run);
    void stopAwakingThread();

private slots:
    void connectToPort();
    void disconnectFromPort();

    void checkPortInput(const QString& text);
    void writeData();

    void exitFromApp();
    void showAbout();

private:
    boost::shared_ptr<Ui::VUnirobotMainWindow> m_ui;
    boost::shared_ptr<VUnirobotThread> m_unirobotThread;
    boost::shared_ptr<boost::thread>   m_updateUiThread;
    boost::shared_ptr<boost::thread>   m_awakingThread;

    std::vector<QSpinBox*>  m_engineSpinBoxContainer;
    std::vector<QCheckBox*> m_engineCheckBoxContainer;

    bool m_updateUiThreadRunning;
    bool m_awakingThreadRunning;
};

#endif // _V_UNIROBOT_MAINWINDOW_H_
