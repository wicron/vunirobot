/*
 *  Copyright (c) 2012 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// ------------------------------------------------------------------- PLATFORM
#include <vcore/platform.h>
// ------------------------------------------------------------------- INCLUDES
#include <iostream>

#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>

#include <QMessageBox>

#include <vunirobot/v_unirobot_adapter.h>
#include <vunirobot/v_unirobot_thread.h>
// ------------------------------------------------------------------- SYNOPSIS
#include "v_unirobot_about_dialog.h"

#include "v_unirobot_mainwindow.h"
#include <ui_v_unirobot_mainwindow.h>
// ----------------------------------------------------------------------------

const int UPDATE_UI_CYCLE_SLEEP_TIME = 250; // ms
const int AWAKING_CYCLE_TIME         = 100; // ms

VUnirobotMainWindow::VUnirobotMainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_updateUiThreadRunning(false),
    m_awakingThreadRunning(false)
{
    m_ui.reset(new Ui::VUnirobotMainWindow);
    m_ui->setupUi(this);
    m_ui->widget->setDisabled(true);

    m_unirobotThread.reset(new vunirobot::VUnirobotThread);

    m_ui->pushButtonConnect->setDisabled(true);
    m_ui->pushButtonDisconnect->setDisabled(true);

    connect(m_ui->lineEditPortName, SIGNAL(textChanged(QString)), this, SLOT(checkPortInput(QString)));

    connect(m_ui->pushButtonConnect,    SIGNAL(clicked()), this, SLOT(connectToPort()));
    connect(m_ui->pushButtonDisconnect, SIGNAL(clicked()), this, SLOT(disconnectFromPort()));

    connect(m_ui->pushButtonWrite, SIGNAL(clicked()), this, SLOT(writeData()));

    connect(m_ui->actionExit,  SIGNAL(triggered()), this, SLOT(exitFromApp()));
    connect(m_ui->actionAbout, SIGNAL(triggered()), this, SLOT(showAbout()));

    m_ui->tableWidget->setColumnCount(1);
    m_ui->tableWidget->setRowCount(vunirobot::SENSORS_NUM);

    m_ui->lineEditPortName->setFocus();

#ifdef OS_LINUX
    m_ui->lineEditPortName->setText("/dev/unirobot");
#endif // OS_LINUX

    m_ui->pushButtonConnect->setFocus();

    m_engineSpinBoxContainer.push_back(m_ui->spinBoxEngine0);
    m_engineSpinBoxContainer.push_back(m_ui->spinBoxEngine1);
    m_engineSpinBoxContainer.push_back(m_ui->spinBoxEngine2);
    m_engineSpinBoxContainer.push_back(m_ui->spinBoxEngine3);

    m_engineCheckBoxContainer.push_back(m_ui->checkBoxEngine0);
    m_engineCheckBoxContainer.push_back(m_ui->checkBoxEngine1);
    m_engineCheckBoxContainer.push_back(m_ui->checkBoxEngine2);
    m_engineCheckBoxContainer.push_back(m_ui->checkBoxEngine3);

    for(size_t i = 0; i < m_engineSpinBoxContainer.size(); i++) {
        m_engineSpinBoxContainer[i]->setMaximum(vunirobot::VUnirobotAdapter::MOTOR_SPEED_MAX);
        m_engineSpinBoxContainer[i]->setMinimum(vunirobot::VUnirobotAdapter::MOTOR_SPEED_MIN);
    }
}

VUnirobotMainWindow::~VUnirobotMainWindow()
{
    stopUpdateUiThread();
}

void VUnirobotMainWindow::startUpdateUiThread()
{
    assert(!m_updateUiThreadRunning);
    m_updateUiThreadRunning = true;
    m_updateUiThread = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&VUnirobotMainWindow::updatedUiThreadFun, this, m_unirobotThread, boost::cref(m_updateUiThreadRunning))));
}

void VUnirobotMainWindow::updatedUiThreadFun(const boost::shared_ptr<VUnirobotThread>& thread, const bool& run)
{
    while(run) {
        vunirobot::sensors_data data;
        bool readSenors = thread->getSenorsValue(data);

        if(readSenors) {
            size_t size = data.size();
            for(size_t i = 0; i < size; i++) {
                QTableWidgetItem* newItemValue = new QTableWidgetItem(tr("%1").arg(data[i]));
                m_ui->tableWidget->setItem(i, 0, newItemValue);
            }
        }
        else {
            int readError = m_ui->spinBoxReadErrors->value();
            int newReadError = readError + 1;
            m_ui->spinBoxReadErrors->setValue(newReadError);
        }

        boost::this_thread::sleep(boost::posix_time::milliseconds(UPDATE_UI_CYCLE_SLEEP_TIME));
    }
}

void VUnirobotMainWindow::stopUpdateUiThread()
{
    m_updateUiThreadRunning = false;
    if(m_updateUiThread) {
        m_updateUiThread->join();
        m_updateUiThread.reset();
    }
}

void VUnirobotMainWindow::startAwakingThread()
{
    assert(!m_awakingThreadRunning);
    m_awakingThreadRunning = true;
    m_awakingThread = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&VUnirobotMainWindow::awakingThreadFun, this, m_unirobotThread, boost::cref(m_awakingThreadRunning))));

}

void VUnirobotMainWindow::awakingThreadFun(const boost::shared_ptr<VUnirobotThread>& thread, const bool& run)
{
    while(run) {
        thread->sleep(false);
        boost::this_thread::sleep(boost::posix_time::milliseconds(AWAKING_CYCLE_TIME));
    }
}

void VUnirobotMainWindow::stopAwakingThread()
{
    m_awakingThreadRunning = false;
    if(m_awakingThread) {
        m_awakingThread->join();
        m_awakingThread.reset();
    }
}

void VUnirobotMainWindow::connectToPort()
{
    QString port(m_ui->lineEditPortName->text());

    bool connect = true;

#ifdef OS_LINUX
    //connect = boost::filesystem::exists(port.toStdString());
#endif // OS_LINUX

    try {
        m_unirobotThread->connectToPort(std::string(port.toStdString().c_str()));
    }
    catch(...) {
        std::cerr << "connect to unirobot error" << std::endl;
    }

    if(connect) {
        startUpdateUiThread();
        startAwakingThread();

        m_ui->pushButtonConnect->setDisabled(true);
        m_ui->lineEditPortName->setDisabled(true);
        m_ui->pushButtonDisconnect->setEnabled(true);
        m_ui->widget->setEnabled(true);
    }
    else {
        m_unirobotThread->disconnectFromPort();

        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Error"));
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText(tr("Can not connect to port '%1'.").arg(port));
        msgBox.exec();
    }
}

void VUnirobotMainWindow::disconnectFromPort()
{
    stopUpdateUiThread();
    stopAwakingThread();

    m_unirobotThread->disconnectFromPort();

    m_ui->spinBoxReadErrors->setValue(0);

    m_ui->pushButtonConnect->setEnabled(true);
    m_ui->lineEditPortName->setEnabled(true);
    m_ui->pushButtonDisconnect->setDisabled(true);
    m_ui->widget->setDisabled(true);
}

void VUnirobotMainWindow::checkPortInput(const QString& text)
{
    if(text.size()) {
        m_ui->pushButtonConnect->setEnabled(true);
    }
    else {
        m_ui->pushButtonConnect->setDisabled(true);
    }
}

void VUnirobotMainWindow::writeData()
{
    vunirobot::task speedTask;
    for(size_t i = 0; i < m_engineSpinBoxContainer.size(); i++) {
        int value = m_engineSpinBoxContainer[i]->value();
        speedTask.insert(std::make_pair(i, value));
    }

    vunirobot::task directionTask;
    for(size_t i = 0; i < m_engineCheckBoxContainer.size(); i++) {
        int state = m_engineCheckBoxContainer[i]->checkState();
        int direction =  state == Qt::Checked ? VUnirobotAdapter::REVERSE : VUnirobotAdapter::DIRECT;
        directionTask.insert(std::make_pair(i, direction));
    }

    bool setSpeed = m_unirobotThread->setMotorSpeed(speedTask);
    bool setDirection = m_unirobotThread->setMotorDirection(directionTask);

    if(setSpeed && setDirection) {
        m_ui->labelWrite->setText(tr("ok"));
    }
    else {
        m_ui->labelWrite->setText(tr("error"));
    }
}

void VUnirobotMainWindow::exitFromApp()
{
    close();
}

void VUnirobotMainWindow::showAbout()
{
    VUnirobotAboutDialog dialog(this);
    dialog.exec();
}
