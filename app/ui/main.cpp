/*
 *  Copyright (c) 2011 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// ----------------------------------------------------------------------- INCLUDES
#include <QTextCodec>
#include <QApplication>

#include <vcore/platform.h>
// ----------------------------------------------------------------------- SYNOPSIS
#include "v_unirobot_mainwindow.h"

#ifdef COMPILER_MSVC
#   include <windows.h>
#endif // COMPILER_MSVC
// --------------------------------------------------------------------------------

#ifdef COMPILER_MSVC

INT WINAPI wWinMain(HINSTANCE hInst, HINSTANCE hPrevInstance, LPWSTR, INT)
{
    UNREFERENCED_PARAMETER(hInst);
    UNREFERENCED_PARAMETER(hPrevInstance);

    int argc;
    char** argv;
    {
        LPWSTR* lpArgv = CommandLineToArgvW( GetCommandLineW(), &argc );
        argv = (char**) malloc( argc*sizeof(char*) );
        int size, i = 0;
        for( ; i < argc; ++i )
        {
            size = wcslen( lpArgv[i] ) + 1;
            argv[i] = (char*) malloc( size );
            wcstombs( argv[i], lpArgv[i], size );
        }
        LocalFree( lpArgv );
    }

#else

int main(int argc, char *argv[])
{
#endif // COMPILER_MSVC

    QApplication a(argc, argv);
    QCoreApplication::setOrganizationName("Wicron");
    QCoreApplication::setOrganizationDomain("wicron.com");
    QCoreApplication::setApplicationName("vdyna-ui-app");

    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);

    VUnirobotMainWindow window;
    window.showMaximized();

    int code = a.exec();

#ifdef COMPILER_MSVC
    {
        int i = 0;
        for( ; i < argc; ++i ) {
            free( argv[i] );
        }
        free( argv );
    }
#endif

    return code;
}
